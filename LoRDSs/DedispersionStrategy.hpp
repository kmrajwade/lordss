#ifndef LORDSS_DEDISPERSIONSTRATEGY_HPP
#define LORDSS_DEDISPERSIONSTRATEGY_HPP

/* A class that creates the list of frequency channels and DM trials for the search */
/* Takes various inputs for the survey*/

#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include <numeric>
#include <string.h>
#include <vector>

namespace lordss{

class DedispersionStrategy
{
    public:

        DedispersionStrategy(std::size_t nchans, float tsamp, float fcen, float BW, float DM_min, float DM_max, std::size_t minram, std::size_t steps);
        ~DedispersionStrategy();
        std::vector<float> const& dms();
        std::vector<float> const& freqs();
        std::size_t const& totalsteps();
        std::size_t const& imsize();
        float const& tsamp();
        void imsize(std::size_t);

        /* Compute how many timesteps can be loaded on RAM */
        void gettimesteps();

    private:
        std::vector<float> _dms;
        std::vector<float> _freqs;
        float _dDM;
        std::size_t _nchans;
        float _tsamp;
        float _bw;
        float _fcen;
        float _dm_min;
        float _dm_max;
        std::size_t _totalsteps;
        std::size_t _imsize;
        std::size_t _minram;
        std::size_t _steps;
};
} //lordss
#endif	
