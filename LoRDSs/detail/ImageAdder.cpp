#include "LoRDSs/ImageAdder.hpp"
#include "LoRDSs/FitsKeyWordReader.hpp"
#include <bits/stdc++.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fstream>
#include <math.h>
#include <immintrin.h>
#include <emmintrin.h>
#include <omp.h>
#include <cassert>
#include "CCfits/CCfits"


namespace lordss
{


void writeImage(std::string const& filename, std::valarray<float>& image, long x, long y, FitsKeyWordReader& reader)
{
    // Create a FITS primary array containing a 2-D image
    //     // declare axis arrays.
    long naxis    =   2;
    long naxes[2] = {x, y};
 
    // declare auto-pointer to FITS at function scope. Ensures no resources
    //     // leaked if something fails in dynamic allocation.
    std::unique_ptr<CCfits::FITS> pFits;
    try
    {    
        // Create a new FITS object, specifying the data type and axes for the primary
        // image. Simultaneously create the corresponding file.
        pFits.reset( new CCfits::FITS(filename , FLOAT_IMG , naxis , naxes ) );
    }
    catch (CCfits::FITS::CantCreate)
    {
        return;
    }

    long nelements = x*y;

    // The function PHDU& FITS::pHDU() returns a reference to the object representing
    //  the primary HDU; PHDU::write( <args> ) is then used to write the data..
    reader.writeheader(pFits->pHDU());
    pFits->pHDU().write(1,nelements,image);
    return;
}

std::size_t getdmstep(std::size_t timestep, std::vector<float> const& delays, std::size_t nchans)
{
    auto iter = std::find(delays.begin(), delays.end(), (float) timestep);
    auto dist = std::distance(delays.begin(), iter);
    if (dist % nchans != 0)
    {
        throw(std::runtime_error("Required delay is too large for the number of time steps. Please change your DM range"));
    }
    return *(iter);
}

template<typename NumRepType>
ImageAdder<NumRepType>::ImageAdder(std::size_t timesteps, std::size_t nchans, DedispersionStrategy& strategy, bool order)
: _timesteps(timesteps)
, _nchans(nchans)
, _strategy(strategy)
, _freqorder(order)	
{
}

template<typename NumRepType>
ImageAdder<NumRepType>::~ImageAdder()
{
}

template<typename NumRepType>
void ImageAdder<NumRepType>::add(ImageData<NumRepType>& image1, ImageData<NumRepType>& image2)
{

	// Adding the images using AVX2 instructions
	std::size_t bytealign_factor = 256/sizeof(NumRepType)/8;

        // make sure that the image sizes are right
	assert(image1.size() == image2.size());

    if (bytealign_factor == 8)
    {
        for (std::size_t ii=0 ; ii < (std::size_t)image2.size()/sizeof(NumRepType)/bytealign_factor ; ++ii )
        {
            __m256 kA   = _mm256_loadu_ps(reinterpret_cast<float*>(&(image1.data()[ii*bytealign_factor])));
            __m256 kB  = _mm256_loadu_ps(reinterpret_cast<float*>(&(image2.data()[ii*bytealign_factor])));
            __m256 kRes = _mm256_add_ps( kA, kB );
            _mm256_storeu_ps( reinterpret_cast<float*>(&(image1.data()[ii*bytealign_factor])),kRes);

        }
    }
    if (bytealign_factor == 4)
    {
        for (std::size_t ii=0 ; ii < (std::size_t)image2.size()/sizeof(NumRepType)/bytealign_factor ; ++ii )
        {
            __m256d kA   = _mm256_loadu_pd(reinterpret_cast<double*>(&(image1.data()[ii*bytealign_factor])));
            __m256d kB  = _mm256_loadu_pd(reinterpret_cast<double*>(&(image2.data()[ii*bytealign_factor])));
            __m256d kRes = _mm256_add_pd( kA, kB );
            _mm256_storeu_pd( reinterpret_cast<double*>(&(image1.data()[ii*bytealign_factor])),kRes);

        }
    }

}


template<typename NumRepType>
void ImageAdder<NumRepType>::start(std::vector<float> const& delays, std::vector<ImageData<NumRepType>>& images, std::string outdir)
{
    // Some pragma parallel for here
    // Make sure we can load all the dmsteps in there
    try
    {
        // Creat a fits header object
        std::size_t maximumtshift = (std::size_t) delays.at(_nchans*_strategy.dms().size() - 1);
        std::size_t dmsteps = _strategy.dms().size();
        if (maximumtshift > _timesteps)
            dmsteps = getdmstep(_timesteps, delays, _nchans);

        #pragma omp parallel for num_threads(16)
        for (std::size_t jj = 0; jj < dmsteps; ++jj)
        {
            std::size_t  maxtstep = (std::size_t) delays.at((jj+1)*_nchans -1);

            std::size_t totaltimesteps = _timesteps - maxtstep;

            std::cout << "total timesteps: " << _timesteps << " reduced timesteps: " << totaltimesteps << std::endl;
            for (std::size_t kk=0; kk < totaltimesteps; ++kk)
            {

                std::string timestep = std::to_string(kk);
                std::string dmtrial = std::to_string(_strategy.dms()[jj]);
                std::stringstream outfile;
		std::size_t ind = 0;
                ImageData<NumRepType> temp_image(images[0].size());
                for (std::size_t ii=0; ii < _nchans; ++ii)
                {
                    //going from high to low frequency but the array is arranged from low to high
		    if (_freqorder)
                    {
                        ind = (_nchans -1 - ii)*_timesteps + (std::size_t)delays.at(ii + jj*_nchans) + kk;
                    }
                    if (!_freqorder)
                    {
                        ind = (_nchans -1 -ii) + _nchans*(kk + (std::size_t)delays.at(ii + jj*_nchans));
                    }
                    assert(ind < images.size());
                    add(temp_image, images[ind]);
                }

		// Divide by the number of channels
		std::transform(std::begin(temp_image.data()), std::end(temp_image.data()), std::begin(temp_image.data()), [&](float n){ return n/_nchans; });
                outfile << outdir << "dedispersed_" << dmtrial << "_" << timestep << ".fits";
                writeImage(outfile.str(), temp_image.data(), temp_image.x(), temp_image.y(), images[kk].metadata());
            }
        }
    }

    catch(...)
    {
        throw;
    }
}
} // lordss
