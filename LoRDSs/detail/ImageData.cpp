#include "LoRDSs/ImageData.hpp"
#include <array>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <CCfits/CCfits>


namespace lordss
{


template<typename NumRepType>
ImageData<NumRepType>::ImageData()
: _x(0)
, _y(0)
{
    _data.resize(0);
}

template<typename NumRepType>
ImageData<NumRepType>::ImageData(std::string filename)
{
    /* Read in the fits file */
    try
    {
        readimage(filename);
    }
    catch(...)
    {
        throw;
    }

}

template<typename NumRepType>
ImageData<NumRepType>::ImageData(std::size_t size)
: _size(size)
{
    _data.resize(size/sizeof(NumRepType));
    _x = (std::size_t) sqrt(size/sizeof(NumRepType));
    _y = (std::size_t) sqrt(size/sizeof(NumRepType));
    _size = size;
    if (_x != _y)
        throw(std::length_error("x-axis and y-axis mismatch. Image should have the same pixel sizes"));
    std::fill(std::begin(_data), std::end(_data), 0.0);
}

template<typename NumRepType>
ImageData<NumRepType>::~ImageData()
{
}

template<typename NumRepType>
std::size_t const& ImageData<NumRepType>::size()
{
    return _size;
}

template<typename NumRepType>
std::size_t const& ImageData<NumRepType>::x()
{
    return _x;
}
template<typename NumRepType>

std::size_t const& ImageData<NumRepType>::y()
{
    return _y;
}

template<typename NumRepType>
std::valarray<NumRepType>& ImageData<NumRepType>::data()
{
    return _data;
}

template<typename NumRepType>
FitsKeyWordReader& ImageData<NumRepType>::metadata()
{
    return _meta_data;
}

template<typename NumRepType>
void ImageData<NumRepType>::readimage(std::string filename)
{
    CCfits::FITS::setVerboseMode(true);
    try
    {
        std::unique_ptr<CCfits::FITS> pInfile(new CCfits::FITS(filename, CCfits::Read, true));
        CCfits::PHDU& image = pInfile->pHDU();
        _meta_data.readheader(image);
        _x = image.axis(0);
        _y =  image.axis(1);
        _size = (std::size_t)(image.axis(0)* image.axis(1))*sizeof(NumRepType);
        image.read(_data);
    }
    catch(CCfits::FitsException&)
    {
	std::cerr << " Fits Exception Thrown by reading function \n";
    }
                              
}
}// lordss
