#ifndef LORDSS_IMAGEDATA_HPP
#define LORDSS_IMAGEDATA_HPP

#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include <numeric>
#include <string.h>
#include <fstream>
#include <valarray>
#include "LoRDSs/FitsKeyWordReader.hpp"

/* A wrapper class for imaging data. It contains all the relevant information about the size in every dimension and always returns a pointer to the data*/
namespace lordss{ 

template<typename NumRepType>
class ImageData
{
	public:
        ImageData();
        ImageData(std::string filename);
        ImageData(std::size_t size);

        ~ImageData();

        std::size_t const& size();
        std::size_t const& x();
        std::size_t const& y();
        std::valarray<NumRepType>& data();
        FitsKeyWordReader&  metadata();
    private:
        void readimage(std::string filename);

    private:

        std::size_t _size; // This size is always in bytes!!!
        std::valarray<NumRepType> _data;
        std::size_t _x; // naxis1
        std::size_t _y; //naxis2
        FitsKeyWordReader _meta_data; 
};
} // namespace lordss
#include "LoRDSs/detail/ImageData.cpp"
#endif
