#include "LoRDSs/FitsKeyWordReader.hpp"
#include "LoRDSs/common.hpp"

namespace lordss{

FitsKeyWordReader::FitsKeyWordReader()
: _restfreq(1420000000.0)
, _telescope("LOFAR")
, _object("J0000+0000")
, _radesys("ICRS")
, _bunit("Jy/Beam")
, _btype("Intensity")
{
}

FitsKeyWordReader::~FitsKeyWordReader()
{
}

void FitsKeyWordReader::readheader(CCfits::PHDU& hdu)
{
    hdu.readKey("CRVAL3", _restfreq);    
    hdu.readKey("TELESCOP", _telescope);    
    hdu.readKey("OBJECT", _object);    
    hdu.readKey("BTYPE", _btype);    
    hdu.readKey("BUNIT", _bunit);    
    hdu.readKey("CTYPE1", _ctype1);    
    hdu.readKey("CRPIX1", _crpix1);    
    hdu.readKey("CRVAL1", _crval1);    
    hdu.readKey("CTYPE2", _ctype2);    
    hdu.readKey("CRPIX2", _crpix2);    
    hdu.readKey("CRVAL2", _crval2);    
    hdu.readKey("CDELT1", _cdelt1);    
    hdu.readKey("CDELT2", _cdelt2);    
    hdu.readKey("CUNIT1", _cunit1);    
    hdu.readKey("CUNIT2", _cunit2);    
    //hdu.readKey("RADESYS", _radesys);    
    
}

void FitsKeyWordReader::writeheader(CCfits::PHDU& hdu)
{
    hdu.addKey("RESTFRQ", _restfreq, "OBSERVING FREQUENCY (HZ)");
    hdu.addKey("TELESCOP", _telescope, "OBSERVING TELESCOPE");
    hdu.addKey("OBJECT", _object, "OBSERVED SOURCE");
    hdu.addKey("BTYPE", _btype, "QUANTITY OF THE 2D IMAGE");
    hdu.addKey("BUNIT", _bunit, "UNIT OF THE QUANTITY");
    hdu.addKey("RADESYS", _radesys, "CO-ORDINATE SYSTEM");
    hdu.addKey("CTYPE1", _ctype1, "Right ascension angle cosine");
    hdu.addKey("CRPIX1", _crpix1, "");
    hdu.addKey("CRVAL1", _crval1, "");
    hdu.addKey("CDELT1", _cdelt1, "");
    hdu.addKey("CUNIT1", _cunit1, "");
    hdu.addKey("CTYPE2", _ctype2, "Declination angle cosine");
    hdu.addKey("CRPIX2", _crpix2, "");
    hdu.addKey("CRVAL2", _crval2, "");
    hdu.addKey("CDELT2", _cdelt2, "");
    hdu.addKey("CUNIT2", _cunit2, "");
}

float const& FitsKeyWordReader::restfreq()
{
    return _restfreq;
}

std::string const& FitsKeyWordReader::telescope()
{
    return _telescope;
}

std::string const& FitsKeyWordReader::object()
{
    return _object;
}

std::string const& FitsKeyWordReader::radesys()
{
    return _radesys;
}

std::string const& FitsKeyWordReader::btype()
{
    return _btype;
}

std::string const& FitsKeyWordReader::bunit()
{
    return _bunit;
}

}// lordss
