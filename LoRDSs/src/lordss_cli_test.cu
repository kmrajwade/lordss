#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cstdio>
#include <string>
#include <glob.h>
#include "LoRDSs/DedispersionStrategy.hpp"
#include "LoRDSs/DedispersionHandler.cuh"
#include "LoRDSs/ImageData.hpp"
#include "LoRDSs/ImageAdder.hpp"
#include "boost/algorithm/string.hpp"
#include "boost/program_options.hpp"

namespace
{
  const size_t ERROR_IN_COMMAND_LINE = 1;
  const size_t SUCCESS = 0;
  const size_t ERROR_UNHANDLED_EXCEPTION = 2; 
}

namespace
{
    std::string toUpper(std::string s)
    {
        for(int i=0;i<(int)s.length();i++){s[i]=toupper(s[i]);}
        return s;
    }
    bool compareNat(const std::string& a, const std::string& b)
    {
        if (a.empty())
            return true;
        if (b.empty())
            return false;
        if (std::isdigit(a[0]) && !std::isdigit(b[0]))
            return true;
        if (!std::isdigit(a[0]) && std::isdigit(b[0]))
            return false;
        if (!std::isdigit(a[0]) && !std::isdigit(b[0]))
        {
            if (a[0] == b[0])
                return compareNat(a.substr(1), b.substr(1));
            return (toUpper(a) < toUpper(b));
            //toUpper() is a function to convert a std::string to uppercase.
        }

        // Both strings begin with digit --> parse both numbers
        std::istringstream issa(a);
        std::istringstream issb(b);
        int ia, ib;
        issa >> ia;
        issb >> ib;
        if (ia != ib)
            return ia < ib;

        // Numbers are the same --> remove numbers and recurse
        std::string anew, bnew;
        std::getline(issa, anew);
        std::getline(issb, bnew);
        return (compareNat(anew, bnew));
    }

    std::vector<std::string> glob(const std::string& pattern) 
    {
        using namespace std;
        glob_t glob_result;
        memset(&glob_result, 0, sizeof(glob_result));
        int return_value = glob(pattern.c_str(), GLOB_TILDE, NULL, &glob_result);
        if(return_value != 0) 
        {
            globfree(&glob_result);
            stringstream ss;
            ss << "glob() failed with return_value " << return_value << endl;
            throw std::runtime_error(ss.str());
        }
        vector<string> filenames;
        for(size_t i = 0; i < glob_result.gl_pathc; ++i) 
        {
            filenames.push_back(string(glob_result.gl_pathv[i]));
        }

        // Natural sort
        std::sort(filenames.begin(), filenames.end(), [] (std::string a, std::string b) {
            return compareNat(a,b);
        });
	    globfree(&glob_result);

        return filenames;
    }  

}
using namespace lordss;

int main(int argc, char* argv[])
{
    try
    {
        std::string indir;
        std::string outdir;
        std::string pattern;
        std::size_t nchans;
        float dm_max;
        float dm_min;
        float bw;
        float tsamp;
        float fcen; 
        std::size_t minram;
        std::size_t steps;
	bool order;

        /**
         * Define and parse the program options
         */
        namespace po = boost::program_options;
        po::options_description desc("Options");
        desc.add_options()
            ("help,h", "Print help messages")

            ("maxDM,x", po::value<float>(&dm_max)->default_value(100.0),
             "Maximum DM to make trials")

            ("minDM,l", po::value<float>(&dm_min)->default_value(10.0),
             "Minimum DM to make trials")

            ("steps,s", po::value<std::size_t>(&steps)->default_value(100.0),
             "Number of steps in DM search")

            ("bandwidth,b", po::value<float>(&bw)->default_value(100.0),
             "Bandwidth of the instrument (in MHz )")

            ("inputdir,i", po::value<std::string>(&indir)->required(),
             "path to where the files are located")

            ("pattern,p", po::value<std::string> (&pattern)->required(),
             "Wildcard pattern for globbing the files")

            ("tsamp,t", po::value<float> (&tsamp)->required(),
             "Sampling interval (in seconds)")

            ("channels,c", po::value<std::size_t> (&nchans)->required(),
             "Number of frequency channels")

            ("cenfreq,f", po::value<float> (&fcen)->required(),
             "Centre frequency of the band (in MHz)")

            ("minram,m", po::value<std::size_t> (&minram)->required(),
             "Minimum amount of RAM to be kept free in GB")

            ("outputdir,o", po::value<std::string> (&outdir)->required(),
             "output directory to save the files")

	    ("tforder,r", po::value<bool> (&order)->required(),
             "order of frequency/time: 1 for frequency major, 0 for time");

                    /* Catch Error and program description */
        po::variables_map vm;
        try
        {
            po::store(po::parse_command_line(argc, argv, desc), vm);
            if ( vm.count("help")  )
            {
                std::cout << "LOFAR image plane dedispersion pipeline v0.1"
                    << std::endl << desc << std::endl;
                return SUCCESS;
            }
            po::notify(vm);
        }
        catch(po::error& e)

        {
            std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
            std::cerr << desc << std::endl;
            return ERROR_IN_COMMAND_LINE;
        }

        
        // CLI app over here
        // Generate a DM plan and available memory to decide on the number of timesteps that can be ingested in memory.
        std::cout << "Generating dedispersion strategy..." << std::endl;
        DedispersionStrategy strategy(nchans, tsamp, fcen, bw, dm_min, dm_max, minram, steps);

        // Generate the Handler and get the delays

        std::cout << "Computing dispersion delays" << std::endl;
        DedispersionHandler dedisphandler;
        dedisphandler(strategy);

        // Glob all the files
        std::string filepath = indir + pattern;     
        std::vector<std::string> files = glob(filepath);

        // calculate how many timesteps can be loaded into memory
        ImageData<float> image(files[0]);
        std::size_t timesteps;
        strategy.imsize(image.size());
        strategy.gettimesteps();
        if (strategy.totalsteps() > files.size()/nchans)
            timesteps = files.size()/nchans;
        else
            timesteps = strategy.totalsteps();
        std::cout << "Will process " << timesteps << " number of timesteps..." << std::endl;

        // load data on to the RAM
        bool firstfile=true;
        std::vector<ImageData<float>> images;
        std::cout << "Loading images on to the RAM" << std::endl;
	if (order)
        {
                std::cout << "Reading files in frequency major order" << std::endl;
                for (std::size_t ii=0; ii < nchans; ++ii)
                {
                        for (std::size_t jj=0; jj < timesteps; ++jj)
                        {
                                images.push_back(ImageData<float>(files[jj + ii*(files.size()/nchans)]));
                                if (firstfile)
                                {
                                        firstfile=false;
                                }
                        }
                }
        }
        if (!order)
        {
                std::cout << "Reading files in time major order" << std::endl;
                for (std::size_t ii=0; ii < timesteps; ++ii)
                {
                        for (std::size_t jj=0; jj < nchans; ++jj)
                        {
                                images.push_back(ImageData<float>(files[jj + ii*nchans]));
                                if (firstfile)
                                {
                                        firstfile=false;
                                }
                        }
                }
        }
        // Construct the Image adder
        std::cout << "Running dedispersion..." << std::endl;
        ImageAdder<float> adder(timesteps, nchans, strategy, order);
        
        // Run the pipeline
        std::cout << "Starting pipeline..." << std::endl;
        adder.start(dedisphandler.delays(), images, outdir);

    }
    catch(std::exception& e)
    {
        std::cerr << "Unhandled Exception reached the top of main: "
            << e.what() << ", application will now exit" << std::endl;
        return ERROR_UNHANDLED_EXCEPTION;
    }
    return SUCCESS;

}
