#include "LoRDSs/DedispersionHandler.cuh"
#include <stdio.h>


/* Error check for CUDA */
#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess)
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

__global__ void dedisperse(float* dms, float* freqs, float* delays, int nchans, float tsamp)
{
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    int idy = blockDim.y * blockIdx.y + threadIdx.y;
    delays[idx + idy * nchans] = ((4.15/1000.0) * dms[idy] * ((1/(freqs[idx]*freqs[idx])) - (1/(freqs[0]*freqs[0]))))/tsamp;
    __syncthreads();
}

namespace lordss
{

DedispersionHandler::DedispersionHandler()
{
}

void DedispersionHandler::operator()(DedispersionStrategy& strategy) 
{
    /*Create arrays on the GPU */
    std::size_t delay_size =  strategy.dms().size() * strategy.freqs().size();
    _delays.resize(delay_size);    
    float* d_dms;
    gpuErrchk(cudaMalloc(&d_dms, strategy.dms().size()*sizeof(float)));

    float* d_freqs;
    gpuErrchk(cudaMalloc(&d_freqs, strategy.freqs().size()*sizeof(float)));

    float* d_delays;
    gpuErrchk(cudaMalloc(&d_delays, delay_size * sizeof(float)));

    /* Copy the data over to the GPUs */
    gpuErrchk(cudaMemcpy((void*) d_dms, (void*) strategy.dms().data(), strategy.dms().size()*sizeof(float), cudaMemcpyHostToDevice));
    gpuErrchk(cudaMemcpy((void*) d_freqs, (void*) strategy.freqs().data(), strategy.freqs().size()*sizeof(float), cudaMemcpyHostToDevice));
  
    std::size_t threadsy, blocksy;
    if (strategy.freqs().size() * strategy.dms().size() <= 1024)
    {
        threadsy = strategy.dms().size();
        blocksy = 1;
    }
    else
    {
        threadsy = 1024/strategy.freqs().size();
        blocksy = strategy.dms().size()/threadsy;
    }
    dim3 blockspergrid(1, blocksy);
    dim3 threadsperblock(strategy.freqs().size(), threadsy);
    /* Run the kernel*/

    dedisperse<<<blockspergrid, threadsperblock >>>(d_dms, d_freqs, d_delays, strategy.freqs().size(), strategy.tsamp());
    cudaDeviceSynchronize();
    /*copy the delays back*/
    gpuErrchk(cudaMemcpy((void *) _delays.data(), (void*) d_delays,  delay_size * sizeof(float), cudaMemcpyDeviceToHost));

    /* Free CUDA arrays */
    gpuErrchk(cudaFree(d_dms));
    gpuErrchk(cudaFree(d_freqs));
    gpuErrchk(cudaFree(d_delays));
}


DedispersionHandler::~DedispersionHandler()
{
}

std::vector<float> const& DedispersionHandler::delays() 
{
    return _delays;
} 

} //lordss
