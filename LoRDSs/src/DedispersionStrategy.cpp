#include "LoRDSs/DedispersionStrategy.hpp"
#include <unistd.h>
#include <string>
#include <math.h>
#include <fstream>
#include "sys/types.h"
#include "sys/sysinfo.h"
#include "LoRDSs/common.hpp"

void process_mem_usage(long long& totalPhysMem , long long& physMemUsed)
{
   struct sysinfo memInfo;
   sysinfo (&memInfo);
   totalPhysMem = memInfo.totalram;
   physMemUsed = memInfo.totalram - memInfo.freeram;
}

namespace lordss{

DedispersionStrategy::DedispersionStrategy(std::size_t nchans, float tsamp, float fcen, float BW, float DM_min, float DM_max, std::size_t minram, std::size_t steps)
: _nchans(nchans)
, _tsamp(tsamp)
, _bw(BW)
, _fcen(fcen)
, _dm_min(DM_min)
, _dm_max(DM_max)
, _minram(minram)
, _steps(steps)
{
    try
    {
        // Some basic checks
        if (fcen < BW/2.0)
            throw std::invalid_argument("fcen cannot be less than half of the bandwidth");

        _dDM = tsamp*0.0001205*powf(fcen,3.0)/(0.5*BW);
        std::size_t nDMs = (std::size_t) powf(2.0,ceil(logf(((DM_max - DM_min)/_dDM))/logf(2))); 
        if (nDMs < steps)
        {
            BOOST_LOG_TRIVIAL(info) << "Number of DM steps are too small, using user specified steps...";
            nDMs = (std::size_t) powf(2.0,ceil(logf(steps)/logf(2))); 
            _dDM = (DM_max - DM_min)/nDMs;
        }
        else
            _dDM = (DM_max - DM_min)/nDMs;

        BOOST_LOG_TRIVIAL(info) << "Total number of DM steps: " << nDMs;
        _dms.reserve(nDMs);
        _freqs.reserve(nchans);

        // Populated the DMs
        for (std::size_t ii=0; ii < nDMs; ++ii)
        {
            _dms.push_back(DM_min + _dDM*ii);
        }

        // Populate the freqs
        for (std::size_t ii=0; ii < nchans; ++ii)
        {
            _freqs.push_back((fcen + (BW/2.0) - ii*(BW/nchans))/1000.0);
        }
    }
    catch (...)
    {
        throw;        
    }
}

DedispersionStrategy::~DedispersionStrategy()
{
}

void DedispersionStrategy::gettimesteps()
{
    long long totalPhysMem, physMemUsed;
    process_mem_usage(totalPhysMem, physMemUsed); // total size in kB
    std::size_t remRAM = totalPhysMem - (physMemUsed);
    std::size_t usableRAM;

    // account for the edge case where minimum RAM is too much
    if (remRAM < _minram*1024*1024*1024)
    {
        BOOST_LOG_TRIVIAL(info) << "Minimum RAM required exceeds available RAM. Calculating suitable usable RAM...";
        usableRAM = remRAM - 1024;
    }
    else
        usableRAM = remRAM - (_minram*1024*1024*1024);
    BOOST_LOG_TRIVIAL(info) << "Total RAM: " << totalPhysMem << " bytes " << " Usable RAM: " << usableRAM << " bytes ";
    /* Compute the total time steps that can be loaded*/
    BOOST_LOG_TRIVIAL(info) << "Image size: " << _imsize;
    _totalsteps = (std::size_t) ((usableRAM - (_nchans*_dms.size()*sizeof(float)))/_nchans/_imsize); // to account for some remaining RAM  
    BOOST_LOG_TRIVIAL(info) << "Total number of timesteps that can be loaded: " << _totalsteps;
}

std::vector<float> const& DedispersionStrategy::dms()
{
    return _dms;
}

std::vector<float> const& DedispersionStrategy::freqs()
{
    return _freqs;
}

float const& DedispersionStrategy::tsamp()
{
    return _tsamp;
}

std::size_t const&  DedispersionStrategy::totalsteps()
{
    return _totalsteps;
}
void DedispersionStrategy::imsize(std::size_t imsize)
{
    _imsize=imsize;
}

std::size_t const&  DedispersionStrategy::imsize()
{
    return _imsize;
}

}// lordss
