#ifndef LORDSS_IMAGEADDER_HPP
#define LORDSS_IMAGEADDER_HPP

#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include <numeric>
#include <string.h>
#include "LoRDSs/ImageData.hpp"
#include "LoRDSs/DedispersionStrategy.hpp"

/* A class that adds up images using avx SIMD intrinsics */
/* Takes as input the number of time steps and the number of frequency channels*/

namespace lordss{

template<typename NumRepType>
class ImageAdder
{
    public:
        ImageAdder(std::size_t timesteps, std::size_t nchans, DedispersionStrategy& strategy, bool order);
        ~ImageAdder();

        void start(std::vector<float> const& delays, std::vector<ImageData<NumRepType>>& images, std::string outdir);


    private:
        void add(ImageData<NumRepType>& image1, ImageData<NumRepType>& image2);

    private:
        std::size_t _timesteps;
        std::size_t _nchans;
        std::size_t _tsamps;
        DedispersionStrategy _strategy;
	bool _freqorder;

};
} // lordss
#include "LoRDSs/detail/ImageAdder.cpp"
#endif 

