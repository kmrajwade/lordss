#include "LoRDSs/test/TestFitsKeyWord.hpp"
#include "LoRDSs/FitsKeyWordReader.hpp"
#include <CCfits/CCfits>

namespace lordss{
namespace test{

TestFitsKeyWord::TestFitsKeyWord()
{
}

TestFitsKeyWord::~TestFitsKeyWord()
{
}

void TestFitsKeyWord::SetUp()
{
}

void TestFitsKeyWord::TearDown()
{
}

// Test Constructor
TEST_F(TestFitsKeyWord, test_constructor)
/* Test that the object gets contructed without any issues */
{
    ASSERT_NO_THROW(FitsKeyWordReader reader;);
}

TEST_F(TestFitsKeyWord, test_file_read)
/* Make sure the constructor works fine for a file read */
{
    std::string filename("/data1/rajwade/LoRDSs/LoRDSs/test/src/test_files/P10Hetdex-low-mosaic.fits");
    std::unique_ptr<CCfits::FITS> pfile;
    pfile.reset(new CCfits::FITS(filename, CCfits::Read, true));
    CCfits::PHDU& image = pfile->pHDU();
    FitsKeyWordReader reader;
    reader.readheader(image);
    ASSERT_EQ(reader.restfreq(),143650000.0);
    ASSERT_EQ(reader.telescope(), "LOFAR");
    ASSERT_EQ(reader.object(),"P10Hetdex");
    ASSERT_EQ(reader.radesys(), "ICRS");
    ASSERT_EQ(reader.btype(), "Intensity");
    ASSERT_EQ(reader.bunit(),"JY/BEAM");
}

} // test
} // lordss
