#include "LoRDSs/test/TestImageData.hpp"
#include "LoRDSs/ImageData.hpp"

namespace lordss{
namespace test{

TestImageData::TestImageData()
{
}

TestImageData::~TestImageData()
{
}

void TestImageData::SetUp()
{
}

void TestImageData::TearDown()
{
}

// Test Constructor
TEST_F(TestImageData, test_constructor)
/* Test that the object gets contructed without any issues */
{
    auto size = 4096;
    std::unique_ptr<ImageData<uint8_t>> image_ptr;
    ASSERT_NO_THROW( image_ptr.reset(new ImageData<uint8_t>(size)));
    ASSERT_EQ(image_ptr->size(), 4096);
    ASSERT_EQ(image_ptr->data().size(), 4096);
}

TEST_F(TestImageData, test_file_read)
/* Make sure the constructor works fine for a file read */
{
    std::string filename("/data1/rajwade/LoRDSs/LoRDSs/test/src/test_files/P10Hetdex-low-mosaic.fits");
    std::unique_ptr<ImageData<uint8_t>> image_ptr;
    ASSERT_NO_THROW( image_ptr.reset(new ImageData<uint8_t>(filename)));
}

} // test
} // lordss
