#include "LoRDSs/test/TestDedispersionHandler.cuh"
#include "LoRDSs/DedispersionStrategy.hpp"
#include "LoRDSs/DedispersionHandler.cuh"

namespace lordss{
namespace test{

TestDedispersionHandler::TestDedispersionHandler()
{
}

TestDedispersionHandler::~TestDedispersionHandler()
{
}

void TestDedispersionHandler::SetUp()
{
}

void TestDedispersionHandler::TearDown()
{
}

TEST_F(TestDedispersionHandler, test_delays)
/* Make sure the constructor works fine for a file read */
{
    DedispersionStrategy strategy(16, 1.0, 150.0, 100.0, 1.0, 512.0, 2, 10);
    DedispersionHandler handler;
    ASSERT_NO_THROW(handler(strategy));   
    std::vector<float> delays1(64*16);
    for (std::size_t ii=0; ii < 64; ++ii)
    {
        for (std::size_t jj=0; jj < 16; ++jj)
        {
            delays1[jj + ii*16] = (4.15/1000.0)*strategy.dms()[ii]*(1/(strategy.freqs()[jj]*strategy.freqs()[jj]) - 1/(strategy.freqs()[0]*strategy.freqs()[0]));
        }
    }   

    std::vector<float> delays2 = handler.delays();

    for (std::size_t ii=0; ii < delays2.size(); ++ii)
    {
        ASSERT_EQ(delays1[ii], delays2[ii]);
    }
}



} //test
} //lordss
