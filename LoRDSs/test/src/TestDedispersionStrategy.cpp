#include "LoRDSs/test/TestDedispersionStrategy.hpp"
#include "LoRDSs/DedispersionStrategy.hpp"

namespace lordss{
namespace test{

TestDedispersionStrategy::TestDedispersionStrategy()
{
}

TestDedispersionStrategy::~TestDedispersionStrategy()
{
}

void TestDedispersionStrategy::SetUp()
{
}

void TestDedispersionStrategy::TearDown()
{
}

// Test Constructor
TEST_F(TestDedispersionStrategy, test_getters)
/* Test all the getters */
{
    DedispersionStrategy strategy(1024, 1.0, 500, 400, 0, 512, 2, 4);
    ASSERT_DOUBLE_EQ(strategy.tsamp(),1);
    ASSERT_EQ(strategy.dms().size(), 8);
    ASSERT_EQ(strategy.freqs().size(), 1024);
}

TEST_F(TestDedispersionStrategy, test_timesteps_calc)
/* Make sure the constructor works fine for a file read */
{
    DedispersionStrategy strategy(1024, 1.0, 500, 400, 0, 512, 2, 100);
    ASSERT_NO_THROW(strategy.gettimesteps());   
    ASSERT_EQ(strategy.dms().size(), 100);
}

TEST_F(TestDedispersionStrategy, test_dms_freqs)
/* Test that the values computed are as expected */
{
    std::vector<float> freqs;
    std::vector<float> dms;
    float dm_min = 10.0;
    float dm_max = 100.0;
    float fcen = 500.0;
    float BW = 50.0;
    float tsamp = 1.0;
    std::size_t nchans = 256;

    float dDM = tsamp*0.0001205*powf(fcen,3.0)/(0.5*BW); 
    std::size_t nDMs = (std::size_t) powf(2.0,ceil(logf(((dm_max - dm_min)/dDM))/logf(2)));
    dDM = (dm_max - dm_min)/nDMs;

    for (std::size_t ii=0; ii < nchans; ++ii )
    {
        freqs.push_back((fcen + (BW/2.0) - ii*(BW/nchans))/1000.0);
    }

    for (std::size_t ii=0; ii < nDMs; ++ii)
    {
        dms.push_back(dm_min + ii*dDM);
    }

    DedispersionStrategy strategy(256, 1.0, 500.0, 50.0, 10.0, 100.0, 2.0, 1);

    for (std::size_t ii=0; ii < nchans; ++ii)
    { 
        ASSERT_EQ(strategy.freqs()[ii], freqs[ii]);
    }

    for (std::size_t ii=0; ii < nDMs; ++ii)
    {
        ASSERT_EQ(strategy.dms()[ii], dms[ii]);
    }
}


} //test
} //lordss
