#include "LoRDSs/test/TestImageAdder.hpp"
#include "LoRDSs/DedispersionStrategy.hpp"
#include "LoRDSs/ImageAdder.hpp"
#include "LoRDSs/common.hpp"

namespace lordss{
namespace test{

TestImageAdder::TestImageAdder()
{
}

TestImageAdder::~TestImageAdder()
{
}

void TestImageAdder::SetUp()
{
}

void TestImageAdder::TearDown()
{
}

// Test Constructor
TEST_F(TestImageAdder, test_implementation)
{
    DedispersionStrategy strategy(16, 1.0, 150, 100, 0.0, 400, 2, 1);
    std::size_t size = 1048576;
    strategy.imsize(size*sizeof(float));
    ASSERT_EQ(strategy.imsize(), 1048576*sizeof(float));
    strategy.gettimesteps();
    std::vector<ImageData<float>> images;
    std::vector<float> delays(strategy.dms().size()*strategy.freqs().size());
    std::fill(delays.begin(), delays.end(), 0.0);
   
    for (std::size_t jj=0; jj < 16; ++jj)
    {
        for (std::size_t ii=0; ii < strategy.totalsteps(); ++ii)
        {
            images.push_back(ImageData<float>(size));
        }
    }

    BOOST_LOG_TRIVIAL(info) << "number of freq channels: " << strategy.freqs().size();
    ImageAdder<float> adder(strategy.totalsteps(), strategy.freqs().size(), strategy, 1);

    ASSERT_NO_THROW(adder.start(delays, images, "./"));
}

} //test
} //lordss
