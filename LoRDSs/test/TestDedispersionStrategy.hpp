#ifndef LORDSS_TESTDEDISPERSIONSTRATEGY_HPP
#define LORDSS_TESTDEDISPERSIONSTRATEGY_HPP

#include <gtest/gtest.h>

namespace lordss{
namespace test{

class TestDedispersionStrategy: public testing::Test
{
    protected:
        void SetUp() override;
        void TearDown() override;

    public:
        TestDedispersionStrategy();
        ~TestDedispersionStrategy();

};
}
}
#endif
