#ifndef LORDSS_TESTIMAGEDATA_HPP
#define LORDSS_TESTIMAGEDATA_HPP

#include <gtest/gtest.h>

namespace lordss{
namespace test{

class TestImageData: public testing::Test
{
    protected:
        void SetUp() override;
        void TearDown() override;

    public:
        TestImageData();
        ~TestImageData();

};
}
}
#endif
