#ifndef LORDSS_TESTIMAGEADDER_HPP
#define LORDSS_TESTIMAGEADDER_HPP

#include <gtest/gtest.h>
#include "LoRDSs/DedispersionStrategy.hpp"

namespace lordss{
namespace test{

class TestImageAdder: public testing::Test
{
    protected:
        void SetUp() override;
        void TearDown() override;

    public:
        TestImageAdder();
        ~TestImageAdder();

};
}
}
#endif
