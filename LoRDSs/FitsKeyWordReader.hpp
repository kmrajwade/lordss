#ifndef LORDSS_FITSKEYWORDREADER_HPP
#define LORDSS_FITSKEYWORDREADER_HPP

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <CCfits/CCfits>

/* A class to read and write important FITS keywords. This class is specifically made for reading LOFAR FITS files*/
namespace lordss{ 

class FitsKeyWordReader
{
	public:
        FitsKeyWordReader();
        ~FitsKeyWordReader();

        void readheader(CCfits::PHDU& hdu);
        void writeheader(CCfits::PHDU& hdu);

        // All the getters
        float const& restfreq();
        std::string const& telescope();
        std::string const& object();
        std::string const& radesys();
        std::string const& bunit();
        std::string const& btype();

    private:
        float _restfreq;
        std::string _telescope;
        std::string _object;
        std::string _radesys;
        std::string _bunit;
        std::string _btype;
        std::string _ctype1;
        std::string _ctype2;
        std::string _cunit1;
        std::string _cunit2;
        float _crpix1;
        float _crpix2;
        float _crval1;
        float _crval2;
        float _cdelt1;
        float _cdelt2;
};
} // namespace lordss
#endif
