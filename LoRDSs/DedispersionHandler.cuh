#ifndef LORDSS_DEDISPERSIONHANDLER_HPP
#define LORDSS_DEDISPERSIONHANDLER_HPP

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include <numeric>
#include <string.h>
#include "LoRDSs/DedispersionStrategy.hpp"

/* A class to generate delays for a given set of DMs and frequencies */

namespace lordss{

class DedispersionHandler
{
	public:
		DedispersionHandler();
		~DedispersionHandler();
 		void operator()(DedispersionStrategy&);
		std::vector<float> const& delays();
		
	private:
		std::vector<float> _delays;
};
} // namespace lordss
#endif
