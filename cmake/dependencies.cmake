#include(cuda)
include(compiler_settings)
include(cmake/thirdparty.cmake)
set(THIRDPARTY_DIR "${PROJECT_SOURCE_DIR}/thirdparty")
set(THIRDPARTY_BINARY_DIR "${CMAKE_BINARY_DIR}/thirdparty")

include(cmake/googletest.cmake)
include(cmake/boost.cmake)
include(cmake/cuda.cmake)
include(cmake/cfitsio.cmake)
include(cmake/ccfits.cmake)
include(cmake/zlib.cmake)
include_directories(SYSTEM ${Boost_INCLUDE_DIR})
include_directories(BEFORE ${CCFITS_INCLUDE_DIR})
include_directories(BEFORE ${CFITSIO_INCLUDE_DIR})
include_directories(BEFORE ${GTEST_INCLUDE_DIR})
include_directories(BEFORE ${ZLIB_INCLUDE_DIR})
set(DEPENDENCY_LIBRARIES
   ${GTEST_LIBRARIES}
   ${Boost_LIBRARIES}
   ${CFITSIO_LIBRARIES}
   ${CCFITS_LIBRARIES}
   ${ZLIB_LIBRARIES}
)


#openmp
find_package(OpenMP REQUIRED)
if(OPENMP_FOUND)
     message(STATUS "Found OpenMP" )
     set(DEPENDENCY_LIBRARIES ${DEPENDENCY_LIBRARIES} ${OpenMP_EXE_LINKER_FLAGS})
     set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
     set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
endif()
