IF (CCFITS_INCLUDE_DIR)
    SET(CCFITS_INC_DIR ${CCFITS_INCLUDE_DIR})
    UNSET(CCFITS_INCLUDE_DIR)
ENDIF (CCFITS_INCLUDE_DIR)

FIND_PATH(CCFITS_INCLUDE_DIR CCfits 
    PATHS ${CCFITS_INC_DIR}
    ${CCFITS_INSTALL_DIR}/include/CCfits
    /usr/local/include/CCfits
    /usr/include/CCfits )

SET(CCFITS_NAMES CCfits)
FOREACH( lib ${CCFITS_NAMES} )
    FIND_LIBRARY(CCFITS_LIBRARY_${lib}
        NAMES ${lib}
        PATHS ${CCFITS_LIBRARY_DIR} ${CCFITS_INSTALL_DIR} ${CCFITS_INSTALL_DIR}/lib /usr/local/lib /usr/lib
        )
    LIST(APPEND CCFITS_LIBRARIES ${CCFITS_LIBRARY_${lib}})
ENDFOREACH(lib)

    # handle the QUIETLY and REQUIRED arguments and set CCFITS_FOUND to TRUE if.
    # all listed variables are TRUE
include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(CCFITS DEFAULT_MSG CCFITS_LIBRARIES CCFITS_INCLUDE_DIR)

IF(NOT CCFITS_FOUND)
    SET( CCFITS_LIBRARIES )
    SET( CCFITS_TEST_LIBRARIES )
ELSE(NOT CCFITS_FOUND)
ENDIF(NOT CCFITS_FOUND)

LIST(APPEND CCFITS_INCLUDE_DIR "${CCFITS_INCLUDE_DIR}")

MARK_AS_ADVANCED(CCFITS_LIBRARIES CCFITS_TEST_LIBRARIES CCFITS_INCLUDE_DIR)

