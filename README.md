# LORDS: LOfar Radio Dedispersion Software
## Overview
A pipeline to dedisperse radio images for over a range of time steps and dispersion measure (DM) trials.
The code uses GPUs to compute the delays over a set of frequencies and DM trials that are decided to be an optimal number by the code. The final dedispesion step is parallelized using a mixture of Advanced Vector Xtentions (AVX2) and OpenMP calls. The code makes use of `googletest` for unit testing is compiled using `cmake` build environment.

## Dependencies
The code does reply on some thirdparty software. The list is as follows:
* `boost` (version 1.63 or above)
* `cmake` (version 3.0 and above)
* `cfitsio` (https://heasarc.gsfc.nasa.gov/fitsio/)
* `CCfits` (https://heasarc.gsfc.nasa.gov/fitsio/CCfits/)

## Installation
Once the dependencies are installed. One can run the following:

* `git clone git clone https://gitlab.com/kmrajwade/lordss.git`
* `cd  lordss`
* `mkdir build`
* `cd build`
* `/software/cmake-3.23.0-rc4/bin/cmake -DENABLE_CUDA=true -DBOOST_ROOT=<path to boost install> -DCCFITS_INCLUDE_DIR=<path to CCfits include dir> -DCCFITS_LIBRARY_CCfits=<path to CCfits library> -DCFITSIO_INCLUDE_DIR=<path to cfitsio installation> -DCFITSIO_LIBRARY_cfitsio=<path to library>  ../` 
* `make -j 16`


